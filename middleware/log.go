package middleware

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"
)

// 自定义日志格式结构体
type logFormatter struct{}

// 实现自定义日志格式接口
func (s *logFormatter) Format(entry *log.Entry) ([]byte, error) {
	timestamp := time.Now().Format("2006-01-02 15:04:05")
	msg := fmt.Sprintf("[%s][%s] %s\n", timestamp, strings.ToUpper(entry.Level.String()), entry.Message)
	return []byte(msg), nil
}

// 初始化日志
func InitLog() {
	// 设置日志格式
	log.SetFormatter(new(logFormatter))
	log.SetReportCaller(true)
	// 日志级别
	log.SetLevel(log.InfoLevel)
}
