package model

import "net/http"

// 通用返回json数据结构体
type RestResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

// 主动抛出异常结构体
type ErrorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// 默认成功信息
func NewSuccess() RestResponse {
	return RestResponse{
		Code:    http.StatusOK,
		Message: "成功",
	}
}

// 填写成功信息
func NewSuccessMessage(message string) RestResponse {
	return RestResponse{
		Code:    http.StatusOK,
		Message: message,
	}
}

// 填写成功数据
func NewSuccessData(data interface{}) RestResponse {
	return RestResponse{
		Code:    http.StatusOK,
		Message: "成功",
		Data:    data,
	}
}

// 填写成功信息、数据
func NewSuccessMessageData(message string, data interface{}) RestResponse {
	return RestResponse{
		Code:    http.StatusOK,
		Message: message,
		Data:    data,
	}
}

// 填写成功编号、信息、数据
func NewSuccessCodeMessageData(code int, message string, data interface{}) RestResponse {
	return RestResponse{
		Code:    code,
		Message: message,
		Data:    data,
	}
}

// 默认错误信息
func NewError() ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusInternalServerError,
		Message: "内部错误",
	}
}

// 填写错误编号
func NewErrorCode(code int) ErrorResponse {
	return ErrorResponse{
		Code:    code,
		Message: "内部错误",
	}
}

// 填写错误信息
func NewErrorMessage(message string) ErrorResponse {
	return ErrorResponse{
		Code:    http.StatusInternalServerError,
		Message: message,
	}
}

// 填写错误编号、错误信息
func NewErrorCodeMessage(code int, message string) ErrorResponse {
	return ErrorResponse{
		Code:    code,
		Message: message,
	}
}
