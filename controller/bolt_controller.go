package controller

import (
	"bolt-manager/model"
	"bolt-manager/service"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"path"
)

// 接收参数结构体
type dbCon struct {
	DbName     string   `json:"dbName"`
	BucketLink []string `json:"bucketLink"`
	Key        string   `json:"key"`
	Value      string   `json:"value"`
	Current    int      `json:"current"`
	Size       int      `json:"size"`
}

// 解析参数
func resolveParam(r *http.Request) dbCon {
	var con dbCon
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(model.NewErrorMessage("参数解析失败"))
	}
	if err = json.Unmarshal(body, &con); err != nil {
		panic(model.NewErrorMessage("参数解析失败"))
	}
	return con
}

// 返回json数据
func responseJson(w http.ResponseWriter, data interface{}) {
	jsonByte, err := json.Marshal(data)
	if err != nil {
		panic(model.NewErrorMessage("结果返回失败"))
	}
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonByte)
	if err != nil {
		panic(model.NewErrorMessage("结果返回失败"))
	}
}

// 获取数据库列表
func QueryDataBaseList(w http.ResponseWriter, r *http.Request) {
	files, err := ioutil.ReadDir(model.DbPath)
	result := make([]string, 0)
	if err == nil {
		for _, f := range files {
			if !f.IsDir() && path.Ext(f.Name()) == ".db" {
				result = append(result, f.Name())
			}
		}
	}
	responseJson(w, model.NewSuccessMessageData("查询成功", result))
}

// 查询存储桶树形结构
func QueryBucketTree(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	result := service.QueryBucketTree(model.DbPath + con.DbName)
	responseJson(w, model.NewSuccessMessageData("查询成功", result))
}

// 分页查询存储桶keys
func QueryBucketKeys(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	result := service.QueryBucketKeys(model.DbPath+con.DbName, con.BucketLink, con.Current, con.Size)
	responseJson(w, model.NewSuccessMessageData("查询成功", result))
}

// 查询指定key的数据
func QueryKeyValue(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	result := service.QueryKeyValue(model.DbPath+con.DbName, con.BucketLink, con.Key)
	responseJson(w, model.NewSuccessMessageData("查询成功", result))
}

// 添加存储桶
func AddBucket(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	service.AddBucket(model.DbPath+con.DbName, con.BucketLink, con.Key)
	responseJson(w, model.NewSuccessMessage("添加成功"))
}

// 删除存储桶
func DeleteBucket(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	service.DeleteBucket(model.DbPath+con.DbName, con.BucketLink)
	responseJson(w, model.NewSuccessMessage("删除成功"))
}

// 添加键值对
func AddBucketKey(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	service.UpdateBucketKey(model.DbPath+con.DbName, con.BucketLink, con.Key, con.Value, true)
	responseJson(w, model.NewSuccessMessage("添加成功"))
}

// 更新键值对
func UpdateBucketKey(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	service.UpdateBucketKey(model.DbPath+con.DbName, con.BucketLink, con.Key, con.Value, false)
	responseJson(w, model.NewSuccessMessage("更新成功"))
}

// 删除键值对
func DeleteBucketKey(w http.ResponseWriter, r *http.Request) {
	con := resolveParam(r)
	service.DeleteBucketKey(model.DbPath+con.DbName, con.BucketLink, con.Key)
	responseJson(w, model.NewSuccessMessage("删除成功"))
}
